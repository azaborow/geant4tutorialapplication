#include "YourEventAction.hh"

#include "G4RunManager.hh"

#include "G4AnalysisManager.hh"

YourEventAction::YourEventAction()
: G4UserEventAction(),
  fEdepPerEvt(0.0) {}


YourEventAction::~YourEventAction() {}


// Beore each event: reset per-event variables
void YourEventAction::BeginOfEventAction(const G4Event* /*anEvent*/) {
  fEdepPerEvt           = 0.0;
}


// After each event:
// fill the histogram with energy deposited in this event
void YourEventAction::EndOfEventAction(const G4Event* /*anEvent*/) {
  // Get analysis manager
  auto analysisManager = G4AnalysisManager::Instance();
  // Fill histogram
  analysisManager->FillH1(0, fEdepPerEvt);
}
