#include "YourSteppingAction.hh"

#include "YourDetectorConstruction.hh"
#include "YourEventAction.hh"

#include "G4Step.hh"


YourSteppingAction::YourSteppingAction(YourDetectorConstruction* det, YourEventAction* evtAction)
:   G4UserSteppingAction(),
    fYourDetector(det),
    fYourEventAction(evtAction) { }


YourSteppingAction::~YourSteppingAction() {}
//
// Score only if the step was done in the Target:
//  - cllect energy deposit for the mean (per-event) energy deposit computation
void YourSteppingAction::UserSteppingAction(const G4Step* theStep) {
  // Score steps done only in the target: i.e. pre-step point was in target
  if (theStep->GetPreStepPoint()->GetTouchableHandle()->GetVolume()
    != fYourDetector->GetTargetPhysicalVolume() )  return;
  // Step was done inside the Target so do scoring:
  //
    // Get the energy deposit
    const G4double eDep   = theStep->GetTotalEnergyDeposit();
    // add current energy deposit to the charged particle track length per-event
    fYourEventAction->AddEnergyDepositPerStep( eDep );
}
