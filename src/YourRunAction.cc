#include "YourRunAction.hh"

#include "YourDetectorConstruction.hh"
#include "YourPrimaryGeneratorAction.hh"
#include "G4Run.hh"

#include "G4AnalysisManager.hh"
#include "G4SystemOfUnits.hh"

#include "Randomize.hh"


YourRunAction::YourRunAction(YourPrimaryGeneratorAction* prim)
:   G4UserRunAction(),
    fYourPrimary(prim) {
    // Create analysis manager
    auto analysisManager = G4AnalysisManager::Instance();
    analysisManager->SetDefaultFileType("csv");
    analysisManager->SetFileName("Hist_Edep.csv");
}

YourRunAction::~YourRunAction() {}

void YourRunAction::BeginOfRunAction(const G4Run*) {
    // Make sure that the Gun position is correct: the user can change the target
    // thickness between construction of objects and start of the run.
    if ( fYourPrimary ) {
        fYourPrimary->UpdatePosition();
    }
    // Get analysis manager
    auto analysisManager = G4AnalysisManager::Instance();
    //
    // Create histogram to store energy
    analysisManager->CreateH1("energy", "Energy", 100, 0, 10 * keV);
    analysisManager->SetH1Ascii(0, true);
    analysisManager->OpenFile();
}


void YourRunAction::EndOfRunAction(const G4Run*) {
  // Scale histogram and write to file
  auto analysisManager = G4AnalysisManager::Instance();
  G4double binWidth = analysisManager->GetH1Width(0);
  G4double factor = 1. / (analysisManager->GetH1(0)->sum_bin_heights() * binWidth);
  analysisManager->ScaleH1(0, factor);
  analysisManager->Write();
  analysisManager->CloseFile();
}
